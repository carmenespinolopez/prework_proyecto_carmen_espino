var experiencia = false;
var estudios = false;
var idiomas = false;

function verExperiencia() {
    var element = document.getElementById('experience');

    if (experiencia == false) {
      element.classList.add("active");
      document.getElementById('studies').classList.remove("active");
      document.getElementById('lenguages').classList.remove("active");
      experiencia = true;
    }
    else {
      element.classList.remove("active");
      experiencia = false;
    }
  }
  
  function verEstudios() {
    var element = document.getElementById('studies');

    if (estudios == false) {
      element.classList.add("active");
      document.getElementById('experience').classList.remove("active");
      document.getElementById('lenguages').classList.remove("active");
      estudios = true;
    }
    else {
      element.classList.remove("active");
      estudios = false;
    }
  }

  function verIdiomas() {
    var element = document.getElementById('lenguages');

    if (idiomas == false) {
      element.classList.add("active");
      document.getElementById('studies').classList.remove("active");
      document.getElementById('experience').classList.remove("active");
      idiomas = true;
    }
    else {
      element.classList.remove("active");
      idiomas = false;
    }
  }
